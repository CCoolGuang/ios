### 什么是block?

> 官方回答:block是一个OC对象，这意味着它能被添加到集合，比如NSArray、NSDictionary

### block的定义
- block属性或变量



```
 /*定义属性，block属性可以用strong修饰，也可以用copy修饰
 @property (nonatomic, copy) void(^myBlock)();//无参无返回值
 @property (nonatomic, copy) void(^myBlock1)(NSString *);//带参数
 @property (nonatomic, copy) NSString *(^myBlock2)(NSString *);//带参数与返回值
 //定义变量
 void(^myBlock)() = nil;//无参无返回值
 void(^myBlock1)(NSString *) = nil;//带参数
 NSString *(^myBlock2)(NSString *) = nil;//带参数与返回值
```


- block当作方法参数

```
 - (void)test:(void(^)())testBlock//无惨无返回值
 - (void)test1:(void(^)(NSString *))testBlock//带参数
 - (void)test2:(NSString *(^)(NSString *))testBlock//带参数与返回值
```

- 使用typedef定义block

```
 typedef void(^myBlock)(); //以后就可以使用myBlock定义无参无返回值的block
 typedef void(^myBlock1)(NSString *); //使用myBlock1定义参数类型为NSString的block
 typedef NSString *(^myBlock2)(NSString *); //使用myBlock2定义参数类型为NSString，返回值也为NSString的block
 //定义属性
 @property (nonatomic, strong) myBlock testBlock;
 //定义变量
 myBlock testBlock = nil;
 //当做参数
 - (void)test:(myBlock)testBlock;
```

### block的赋值

- 格式`block = ^返回值类型(参数列表)`
- 没有参数没有返回值
```
myBlock testBlock = ^void(){
     NSLog(@"test");
 }；
 //没有返回值，void可以省略
myBlock testBlock1 = ^(){
     NSLog(@"test1");
 }；
 //没有参数，小括号也可以省略
 myBlock testBlock2 = ^{
     NSLog(@"test2");
 }；
```
- 有参数没有返回值
```
myBlock1 testBlock = ^void(NSString *str) {
      NSLog(str);
}
//省略void
myBlock1 testBlock = ^(NSString *str) {
      NSLog(str);
}
```
- 有参数有返回值
```
myBlock2 testBlock = ^NSString *(NSString *str) {
     NSLog(str)
     return @"hi";
}
//有返回值时也可以省略返回值类型
 myBlock2 testBlock2 = ^(NSString *str) {
     NSLog(str)
     return @"hi";
}
```

### block使用中注意的问题

- weak指针 : `__weak __typeof__(self) weakSelf = self;`