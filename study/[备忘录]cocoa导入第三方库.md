
- 检测终端`pod`版本 `pod --version`  - 如果现实 `not found`,就要先安装`pod`

- 安装`pod`,输入 `sudo gem install cocoapods`
- 移动路径到当前项目文件下 - `cd Your project`
- 编辑上面创建 Podfile文件 - `vim Podfile` `[大小写不可错]`
- 编辑内容输入

```
target "项目名称" do
   platform:ios,'8.0'
   pod 'AFNetworking','~>2.6.3'
   pod 'Masonry','~>1.0.1'
   [其他需要导入的第三方库]
end
```
- 输入命令 `pod install`

- 运行`.xcworkplace` 文件