#   git使用过程总结

##  1.本地仓库上传文件到git

1)本地克隆远程仓库

- `git clone [ssh连接]`
- `git pull origin [分支名]` -`拉取分支到主干`
- `git add .`-`提交所有更改`
- `git commit -m [注释信息]`
- `git push` - `提交到仓库`  

2)本地连接远程仓库

- `git init` - `初始化本地仓库`
- `git remote add origin [ssh连接]`  - `建立连接`
- `git add .`-`提交所有更改`
- `git commit -m [注释信息]`
- `git push` - `提交到仓库` 

Note:

1. 再pull操作前,一定要查看status,如果有待commit的文件,先push
2. 多个分支可能会导致问题,可以新建分支,分支命令参考:[分支操作](https://www.cnblogs.com/xiaomaomao/p/13886175.html)
