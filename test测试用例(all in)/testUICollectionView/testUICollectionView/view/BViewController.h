//
//  BViewController.h
//  testUICollectionView
//
//

#import <UIKit/UIKit.h>
#import "Masonry.h"
typedef void(^myBlock)(NSString *);
NS_ASSUME_NONNULL_BEGIN

@interface BViewController : UIViewController
@property (nonatomic, strong) UITextField *infoText;
@property (nonatomic, strong) myBlock block;
@end

NS_ASSUME_NONNULL_END
