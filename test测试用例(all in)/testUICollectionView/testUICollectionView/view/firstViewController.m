//
//  firstViewController.m
//  testUICollectionView
//
//

#import "firstViewController.h"
#import "Masonry.h"
@interface firstViewController ()

@end

@implementation firstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor=[UIColor whiteColor];
    [self addButton];
    [self addText];
}
-(void) addText{
    //初始化
    self.lastVisited = [[UITextField alloc]init];
    self.infoText = [[ UILabel alloc]init];
    //增加视图
    [self.view addSubview:self.lastVisited];
    [self.view addSubview:self.infoText];
    //增加限制
    self.infoText.adjustsFontSizeToFitWidth = YES;
    self.infoText.textAlignment = NSTextAlignmentRight;
    self.infoText.text = @"回调小测试:";
    self.infoText.textColor = [UIColor systemBlueColor];
    [self.infoText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(10);
        make.top.equalTo(self.view.mas_top).offset(64);
        make.centerY.equalTo(self.lastVisited.mas_centerY);
        make.width.equalTo(@(80));
    }];
    self.lastVisited.textAlignment = NSTextAlignmentCenter;
    self.lastVisited.adjustsFontSizeToFitWidth = YES;
    self.lastVisited.placeholder = @"访问页面获取的回调信息将会显示在这里";
    self.lastVisited.textColor = [UIColor blackColor];
    self.lastVisited.minimumFontSize = 0.1;
    self.lastVisited.borderStyle = UITextBorderStyleLine;
    self.lastVisited.enabled = NO;
    [self.lastVisited mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.infoText.mas_right).offset(3);
        make.top.equalTo(self.view.mas_top).offset(64);
        make.width.equalTo(@(220));
    }];
    
}
-(void) addButton{
    //初始化
    self.caseOne = [[UIButton alloc]init];
    self.caseTwo = [[UIButton alloc]init];
    self.caseThree = [[UIButton alloc]init];
    //增加视图
    [self.view addSubview:_caseOne];
    [self.view addSubview:_caseTwo];
    [self.view addSubview:_caseThree];
    //初始化文本
    [self.caseOne setTitle:@"点击跳转Case 1" forState:UIControlStateNormal];
    [self.caseTwo setTitle:@"点击跳转Case 2" forState:UIControlStateNormal];
    [self.caseThree setTitle:@"点击跳转Case 3" forState:UIControlStateNormal];
    [self.caseOne setTitleColor:[UIColor systemBlueColor] forState:UIControlStateNormal];
    [self.caseTwo setTitleColor:[UIColor systemBlueColor] forState:UIControlStateNormal];
    [self.caseThree setTitleColor:[UIColor systemBlueColor] forState:UIControlStateNormal];
    //初始化布局
    [self.caseOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.top.equalTo(self.view.mas_top).offset(124);
        make.height.equalTo(@(50));
        make.width.equalTo(@(200));
    }];
    [self.caseTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.caseOne.mas_centerX);
        make.top.equalTo(self.caseOne.mas_bottom).offset(10);
        make.height.equalTo(@(50));
        make.width.equalTo(@(200));
    }];
    [self.caseThree mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.top.equalTo(self.caseTwo.mas_bottom).offset(10);
        make.height.equalTo(@(50));
        make.width.equalTo(@(200));
    }];
    [self.caseOne setTag:1];
    [self.caseOne addTarget:self action:@selector(touchButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.caseTwo setTag:2];
    [self.caseTwo addTarget:self action:@selector(touchButton:) forControlEvents:UIControlEventTouchUpInside];
   
    [self.caseThree setTag:3];
    [self.caseThree addTarget:self action:@selector(touchButton:) forControlEvents:UIControlEventTouchUpInside];

}
-(void) touchButton: (UIButton *) myButton{
    NSInteger index = myButton.tag;

    if(index == 1){
        //申请
        AViewController *temp = [[AViewController alloc]init];
        //回调函数
        __weak __typeof__(self) weakSelf = self;
        temp.block = ^(NSString *tempText){
            if([tempText isEqualToString:@""])
                weakSelf.lastVisited.text = [[NSString alloc]initWithFormat:@"上次访问的界面是case 1"];
            else
                weakSelf.lastVisited.text = [[NSString alloc]initWithFormat:@"你在Case 1页面输入了[%@]",tempText];
        };
        //赋值+跳转
        [temp.navigationItem setTitle:@"Case 1"];
        [self.navigationController pushViewController:temp animated:true];
    }else if(index == 2){
        
        BViewController *temp = [[BViewController alloc]init];
        //回调函数
        __weak __typeof__(self) weakSelf = self;
        temp.block = ^(NSString *tempText){
            if([tempText isEqualToString:@""])
                weakSelf.lastVisited.text = [[NSString alloc]initWithFormat:@"上次访问的界面是case 2"];
            else
                weakSelf.lastVisited.text = [[NSString alloc]initWithFormat:@"你在Case 2页面输入了[%@]",tempText];
        };
        //赋值+跳转
        [temp.navigationItem setTitle:@"Case 2"];
        [self.navigationController pushViewController:temp animated:true];
    }else if(index == 3){
        CViewController *temp = [[CViewController alloc]init];
        //回调函数
        __weak __typeof__(self) weakSelf = self;
        temp.block = ^(NSString *tempText){
            if([tempText isEqualToString:@""])
                weakSelf.lastVisited.text = [[NSString alloc]initWithFormat:@"上次访问的界面是case 3"];
            else
                weakSelf.lastVisited.text = [[NSString alloc]initWithFormat:@"你在Case 3页面输入了[%@]",tempText];
        };
        //赋值+跳转
        [temp.navigationItem setTitle:@"Case 3"];
        [self.navigationController pushViewController:temp animated:true];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
