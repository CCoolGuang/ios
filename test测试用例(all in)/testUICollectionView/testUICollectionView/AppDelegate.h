//
//  AppDelegate.h
//  testUICollectionView
//
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "firstViewController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (readonly, strong) NSPersistentContainer *persistentContainer;
@property (nonatomic, strong) UIWindow *window;

- (void)saveContext;


@end

