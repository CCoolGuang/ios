//
//  rootViewController.h
//  testScrollView
//
//  Created by bytedance on 2021/10/11.
//

#import <UIKit/UIKit.h>
#import "Masonry.h"
#import "slideViewController.h"
#import "enlargeViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface rootViewController : UIViewController
@property (nonatomic, strong) UIButton *sildeButton;
@property (nonatomic, strong) UIButton *enlargeButton;

-(void)addButton;
-(void)didSelectSildeButton;
-(void)didSelectEnlargeButton;
@end

NS_ASSUME_NONNULL_END
