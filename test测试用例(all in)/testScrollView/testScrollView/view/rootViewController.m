//
//  rootViewController.m
//  testScrollView
//
//  Created by bytedance on 2021/10/11.
//

#import "rootViewController.h"
@interface rootViewController ()

@end

@implementation rootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    // Do any additional setup after loading the view.
    [self addButton];
}
-(void)addButton{
    self.sildeButton = [[UIButton alloc]init];
    [self.view addSubview:self.sildeButton];
    [self.sildeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(158);
        make.height.equalTo(@(30));
        make.centerX.equalTo(self.view.mas_centerX);
        make.width.equalTo(self.view.mas_width).multipliedBy(0.8);
    }];
    [self.sildeButton setTitle:@"点击跳转滑动浏览图片页面" forState:UIControlStateNormal];
    [self.sildeButton setTitleColor:[UIColor systemBlueColor] forState:UIControlStateNormal];
    [self.sildeButton addTarget:self action:@selector(didSelectSildeButton) forControlEvents:UIControlEventTouchUpInside];
    
    self.enlargeButton = [[UIButton alloc]init];
    [self.view addSubview:self.enlargeButton];
    [self.enlargeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.sildeButton.mas_bottom).offset(30);
        make.height.equalTo(@(30));
        make.centerX.equalTo(self.view.mas_centerX);
        make.width.equalTo(self.view.mas_width).multipliedBy(0.8);
    }];
    [self.enlargeButton setTitle:@"点击跳转缩放浏览图片页面" forState:UIControlStateNormal];
    [self.enlargeButton setTitleColor:[UIColor systemBlueColor] forState:UIControlStateNormal];
    [self.enlargeButton addTarget:self action:@selector(didSelectEnlargeButton) forControlEvents:UIControlEventTouchUpInside];
}
-(void)didSelectSildeButton{
    NSLog(@"选择了 滑动浏览 按钮");
    slideViewController *slide = [[slideViewController alloc]init];
    [slide.navigationItem setTitle:@"滑动查看图片"];
    [self.navigationController pushViewController:slide animated:true];
}
-(void)didSelectEnlargeButton{
    NSLog(@"选择了 缩放浏览 按钮");
    enlargeViewController *enlarge = [[enlargeViewController alloc]init];
    [enlarge.navigationItem setTitle:@"缩放查看图片"];
    [self.navigationController pushViewController:enlarge animated:true];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
