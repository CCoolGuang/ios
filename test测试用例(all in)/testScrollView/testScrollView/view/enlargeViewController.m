//
//  enlargeViewController.m
//  testScrollView
//
//  Created by bytedance on 2021/10/11.
//

#import "enlargeViewController.h"

@interface enlargeViewController ()
<UIScrollViewDelegate>
@end

@implementation enlargeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.imageView];
}
#pragma mark UIScrollViewDelegate
//指定缩放视图
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imageView;
}
#pragma mark 懒加载初始化
//使用懒加载
-(UIScrollView *) scrollView{
    if(!_scrollView){
        _scrollView = [[UIScrollView alloc]initWithFrame:self.view.bounds];
        _scrollView.backgroundColor = [UIColor whiteColor];
        // 滚动区域大小设置
        _scrollView.contentSize = self.imageView.frame.size;
        // 屏幕旋转时 自动改变布局
        _scrollView.autoresizingMask =  UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        //设置代理
        _scrollView.delegate = self;
        //设置最大倍数 和 最小倍数
        _scrollView.minimumZoomScale = 0.1;
        _scrollView.maximumZoomScale = 4.0;
        //比例因子
        _scrollView.zoomScale = 1.0;
    }
    return _scrollView;
}
//使用懒加载
-(UIImageView *) imageView{
    if(!_imageView){
        UIImage *tempImage = [UIImage imageNamed:@"image"];
        _imageView = [[UIImageView alloc]initWithImage:tempImage];
    }
    return _imageView;
}

@end
