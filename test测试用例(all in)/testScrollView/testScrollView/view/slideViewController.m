//
//  slideViewController.m
//  testScrollView
//
//  Created by bytedance on 2021/10/11.
//

#import "slideViewController.h"

@interface slideViewController ()

@end

@implementation slideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.imageView];
}


#pragma mark 懒加载初始化
//使用懒加载
-(UIScrollView *) scrollView{
    if(!_scrollView){
        _scrollView = [[UIScrollView alloc]initWithFrame:self.view.bounds];
        _scrollView.backgroundColor = [UIColor whiteColor];
        
        // 滚动区域大小设置
        _scrollView.contentSize = self.imageView.frame.size;
        
        // 屏幕旋转时 自动改变布局
        _scrollView.autoresizingMask =  UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _scrollView;
}
//使用懒加载
-(UIImageView *) imageView{
    if(!_imageView){
        UIImage *tempImage = [UIImage imageNamed:@"image"];
        _imageView = [[UIImageView alloc]initWithImage:tempImage];
    }
    return _imageView;
}

@end
