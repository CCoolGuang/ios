//
//  enlargeViewController.h
//  testScrollView
//
//  Created by bytedance on 2021/10/11.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface enlargeViewController : UIViewController
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIImageView *imageView;
@end

NS_ASSUME_NONNULL_END
