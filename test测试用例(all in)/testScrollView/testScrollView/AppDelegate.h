//
//  AppDelegate.h
//  testScrollView
//
//  Created by bytedance on 2021/10/11.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "rootViewController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (readonly, strong) NSPersistentContainer *persistentContainer;
@property (nonatomic, strong) UIWindow *window;
- (void)saveContext;


@end

