//
//  firstViewController.h
//  testUICollectionView
//
//

#import <UIKit/UIKit.h>
#import "AViewController.h"
#import "BViewController.h"
#import "CViewController.h"

typedef void(^myBlock)(NSString *);
NS_ASSUME_NONNULL_BEGIN
@interface firstViewController : UIViewController
@property (nonatomic, strong) myBlock block;
@property (nonatomic, strong) UIButton *caseOne;
@property (nonatomic, strong) UIButton *caseTwo;
@property (nonatomic, strong) UIButton *caseThree;
@property (nonatomic, strong) UILabel *infoText;
@property (nonatomic, strong) UITextField *lastVisited;


@end

NS_ASSUME_NONNULL_END
