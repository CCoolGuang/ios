//
//  CViewController.m
//  testUICollectionView
//
//

#import "CViewController.h"

@interface CViewController ()

@end

@implementation CViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor=[UIColor whiteColor];
    [self addInfoText];
}
-(void)addInfoText{
    //初始化
    self.infoText = [[UITextField alloc]init];
    //增加视图
    [self.view addSubview:self.infoText];
    //增加限制
    self.infoText.adjustsFontSizeToFitWidth = YES;
    self.infoText.textAlignment = NSTextAlignmentCenter;
    self.infoText.placeholder = @"或许你可以在这里输入点什么";
    self.infoText.textColor = [UIColor systemBlueColor];
    [self.infoText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.top.equalTo(self.view.mas_top).offset(64);
        make.width.equalTo(@(200));
    }];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (![[self.navigationController viewControllers] containsObject:self])
    {
        self.block(_infoText.text);
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
