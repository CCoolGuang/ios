//
//  snakeRun.h
//  testNSTimer
//
//  Created by bytedance on 2021/9/18.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN


@interface snakeRun : NSObject

@property(nonatomic,assign) long squareSize; // 蛇身大小
@property(nonatomic,assign) CGSize* runBrder; // 活动边界
@property(nonatomic,assign) long initLength; //初始化长度
@property(nonatomic,assign) CGPoint* headNode; // 头部节点
@property(nonatomic,assign) CGPoint* tailNode; // 尾部节点
@property(nonatomic,assign) int headDirection; // 蛇头方向 0->up 1->down 2->left 3->right
@property(nonatomic,strong) NSMutableArray<UIView*> *viewBody;//蛇身视图
@property(nonatomic,strong) NSMutableArray  *snakeBody; //
@property(nonatomic,strong) NSMutableDictionary* hashBody; // 蛇身映射
-(void)createBody;
-(BOOL)checkInclude:(CGPoint) checkNode; //判断节点是否在活动范围内
-(BOOL)checkLocate:(CGPoint)checkNode; // 判断节点是否已经被占用*
-(BOOL)checkNextNode:(CGPoint)checkNode;
-(CGPoint)getFoodNextNode; //获取食物节点
-(CGPoint)getNextNode :(CGPoint)curNode :(int) direction; // 获取蛇头下一个节点
@end

NS_ASSUME_NONNULL_END
