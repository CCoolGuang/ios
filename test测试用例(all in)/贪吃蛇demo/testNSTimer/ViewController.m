//
//  ViewController.m
//  testNSTimer
//
//  Created by bytedance on 2021/9/18.
//

#import "ViewController.h"
#import "snakeRun.h"
@interface ViewController ()

@property(nonatomic,strong) UIButton *start;
@property(nonatomic,strong) UIView  *runView;
@property(nonatomic,strong) snakeRun *snake;

@end
@implementation ViewController
-(IBAction)startGame:(id)sender{
    [self runRect];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.snake = [[snakeRun alloc]init];
    self.snake.initLength = 10;
    self.snake.headDirection = 3;
    [self.snake createBody];
    [self addView];
    [self addButton];
    for(id tempview in self.snake.viewBody) [self.runView addSubview:tempview];
    [self updateBody];
}

-(void)addButton{
    CGFloat tempWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat tempheight = [UIScreen mainScreen].bounds.size.height;
    //self.start = [[UIButton alloc]initWithFrame:CGRectMake(100, tempheight/4,, 50)];
    self.start = [[UIButton alloc]initWithFrame:CGRectMake(100, 140, 120, 50)];
    self.start.backgroundColor = [UIColor redColor];
    [self.start setTitle:@"开始游戏" forState:UIControlStateNormal];
    [self.start addTarget:self action:@selector(startGame:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.start];
}
-(void)addView{
    CGFloat tempWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat tempheight = [UIScreen mainScreen].bounds.size.height;
    self.runView = [[UIView alloc]initWithFrame:CGRectMake(20,tempheight/2-20, tempWidth-40, tempheight/2-4)];
   // self.runView.backgroundColor = [UIColor systemPinkColor];
    [self.view addSubview:self.runView];
}
-(void)runRect{
    NSTimer* timer = [NSTimer timerWithTimeInterval:0.2 target:self selector:@selector(updateBody) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

-(void)updateBody{
    //删除视图
    [[self.snake.viewBody firstObject]removeFromSuperview];
    [self.snake.viewBody removeObjectAtIndex:0];
    //删除映射
    [self.snake.hashBody removeObjectForKey:NSStringFromCGPoint(CGPointFromString([self.snake.snakeBody firstObject]))];
    //删除节点
    [self.snake.snakeBody removeObjectAtIndex:0];
    // 删除尾节点
    
    CGPoint nextNode = [self.snake getNextNode:CGPointFromString([self.snake.snakeBody lastObject]) :self.snake.headDirection];
    if(nextNode.x == -1){
        NSLog(@"Error!");
    }
    //增加视图
    UIView* tempview = [[UIView alloc]initWithFrame:CGRectMake(nextNode.x, nextNode.y, 14, 14)];
    tempview.backgroundColor = [UIColor blackColor];
    [self.runView addSubview:[self.snake.viewBody lastObject]];
    [self.snake.viewBody addObject:tempview];
    //增加节点
    [self.snake.snakeBody addObject:NSStringFromCGPoint(nextNode)];
    //增加映射
    [self.snake.hashBody setObject:@"Exist" forKey:NSStringFromCGPoint(nextNode)];

    NSLog(@"count 1:%lu count 2:%lu count 3:%lu",[self.snake.hashBody count],[self.snake.snakeBody count],[self.snake.viewBody count]);
    
}
@end
