//
//  SceneDelegate.h
//  testNSTimer
//
//  Created by bytedance on 2021/9/18.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

