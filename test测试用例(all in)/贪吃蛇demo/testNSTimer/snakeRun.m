//
//  snakeRun.m
//  testNSTimer
//
//  Created by bytedance on 2021/9/18.
//

#import "snakeRun.h"
@implementation snakeRun
-(BOOL)checkInclude:(CGPoint)checkNode{
    CGFloat x = checkNode.x;
    CGFloat y = checkNode.y;
    if(x<0 || x>266 || y<0 || y>266) return NO;
    return YES;
}
-(BOOL)checkLocate:(CGPoint)checkNode{
    if([self.hashBody objectForKey:NSStringFromCGPoint(checkNode)])
        return NO;
    return  YES;
}
-(BOOL)checkNextNode:(CGPoint)checkNode{
    if([self checkLocate:checkNode] && [self checkInclude:checkNode]) return YES;
    return NO;
}
-(void)createBody{
    self.hashBody = [NSMutableDictionary dictionaryWithCapacity:40];
    self.viewBody = [NSMutableArray arrayWithCapacity:40];
    self.snakeBody = [NSMutableArray arrayWithCapacity:40];
    int i =0;
    for(i=0;i<self.initLength;i++){
        UIView *tempview = [[UIView alloc]initWithFrame:CGRectMake(i*14,0,  14, 14)];
        tempview.backgroundColor = [UIColor blackColor];
        [self.viewBody addObject:tempview];
        [self.hashBody setObject:@"Exist" forKey:NSStringFromCGPoint(CGPointMake(i*14,0))];
        [self.snakeBody addObject:NSStringFromCGPoint(CGPointMake(i*14,0))];
    }
   // NSLog(@"%lu : %lu",[self.viewBody count],[self.hashBody count]);
    
}
-(CGPoint)getNextNode:(CGPoint)curNode :(int)direction{
    
    CGFloat x = curNode.x;
    CGFloat y = curNode.y;
    //NSLog(@"%@",NSStringFromCGPoint(curNode));
    CGPoint resPoint = CGPointMake(0, 0); // 最终
    BOOL flag = YES;
    NSLog(@"%d",direction);
    int tcot = 0;
    while(flag){
        switch (direction){
            case 2:
                if([self checkNextNode:CGPointMake(x-14,y)]){
                    resPoint = CGPointMake(x-14, y);
                    self.headDirection = 2;
                    flag = NO;
                }
                break;
            case 3:
                if([self checkNextNode:CGPointMake(x+14,y)]){
                    resPoint = CGPointMake(x+14, y);
                    self.headDirection = arc4random()%4;
                    flag = NO;
                }
                break;
            case 0:
                if([self checkNextNode:CGPointMake(x,y-14)]){
                    resPoint = CGPointMake(x, y-14);
                    self.headDirection = 0;
                    flag = NO;
                }
                break;
            case 1:
                if([self checkNextNode:CGPointMake(x,y+14)]){
                    resPoint = CGPointMake(x, y+14);
                    self.headDirection = arc4random()%4;
                    flag = NO;
                }
                break;
            default:
                NSLog(@"Direction Error!");
                break;
        }
        direction = arc4random()%4;
        tcot = tcot + 1;
        if(tcot == 300)
            return CGPointMake(-1, -1);
    }
    
    return resPoint;
}
-(CGPoint)getFoodNextNode{
    
    return  CGPointMake(0, 0);
}
@end
