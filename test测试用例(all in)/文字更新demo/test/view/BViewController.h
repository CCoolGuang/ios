//
//  BViewController.h
//  testNav
//
//  Created by bytedance on 2021/10/9.
//

#import <UIKit/UIKit.h>
typedef void(^myBlock1)(NSString*);
NS_ASSUME_NONNULL_BEGIN

@interface BViewController : UIViewController
@property(nonatomic,strong) UILabel* infoText;
@property(nonatomic,strong) UITextField* inputText;
@property(nonatomic,strong) UIButton* commitButton;
//视图层 增加跳转新的viewController 但是未实现 通过此进行回调
@property(nonatomic,copy) myBlock1 setText;
@end

NS_ASSUME_NONNULL_END
