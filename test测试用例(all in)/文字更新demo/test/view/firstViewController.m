//
//  firstViewController.m
//  testView
//
//  Created by bytedance on 2021/10/9.
//

#import "firstViewController.h"
#import "Masonry.h"
#import "AppDelegate.h"
#import "BViewController.h"
@interface firstViewController ()

@end

@implementation firstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // 工具栏定义
    UILabel *lable = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    lable.text = @"文字展示";
    lable.font = [UIFont boldSystemFontOfSize:14];
    lable.textColor = [UIColor systemBlueColor];
    lable.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView = lable;
    [self addTextLabel];
    [self addUpdateButton];
}
-(void)addTextLabel{
    self.textLabel = [[UILabel alloc]init];
    self.view.backgroundColor = [UIColor whiteColor];
    //增加限制之前 一定要加入视图
    [self.view addSubview:self.textLabel];
    //使用Masonry 增加限制
    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(84);
        make.left.equalTo(self.view.mas_left).offset(10);
        make.right.equalTo(self.view.mas_right).offset(-10);
        make.bottom.equalTo(self.view.mas_bottom).offset(-200);
    }];
    //初始化文本
    self.textLabel.text = @"未获取到任何内容";
    self.textLabel.textAlignment = NSTextAlignmentCenter;
    self.textLabel.backgroundColor = [UIColor systemTealColor];
}
-(void)addUpdateButton{
    self.update = [[UIButton alloc]init];
    //增加限制之前 一定要加入视图
    [self.view addSubview:self.update];
    //使用Masonry 增加限制
    [self.update mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textLabel.mas_bottom).offset(10);
        make.centerX.equalTo(self.view.mas_centerX);
        make.width.equalTo(@(150));
        make.height.equalTo(@(36));
    }];
    //初始化文本
    [self.update setBackgroundColor:[UIColor systemBlueColor]];
    [self.update.layer setMasksToBounds:YES]; // 是否产生阴影
    [self.update.layer setCornerRadius:8.0]; // 圆角弧度
    [self.update setTitle:@"点击更新文本内容" forState:UIControlStateNormal];
    [self.update setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.update addTarget:self action:@selector(updateText) forControlEvents:UIControlEventTouchUpInside];
}
-(void)updateText{
    NSLog(@"点击了按钮");
    __weak __typeof__(self) weakSelf = self;
    BViewController *second = [[BViewController alloc]init];
    //声明该函数块 但未执行 等待回调.
    second.setText = ^(NSString* item){
        weakSelf.textLabel.text = item;
    };
    [self.navigationController pushViewController:second animated:true];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
