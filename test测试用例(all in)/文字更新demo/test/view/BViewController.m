#import "BViewController.h"
#import "Masonry.h"
@interface BViewController ()

@end

@implementation BViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationItem setTitle:@"更新内容"];
    [self addInfoText];
    [self addCommitButton];
}
-(void)addInfoText{
    //初始化
    self.infoText = [[UILabel alloc]init];
    self.inputText = [[UITextField alloc]init];
    //增加视图
    [self.view addSubview:self.infoText];
    [self.view addSubview:self.inputText];
    //增加限制
    [self.infoText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(60);
        make.right.equalTo(self.inputText.mas_left).offset(-10);
        make.top.equalTo(self.view.mas_top).offset(160);
        make.height.equalTo(@(40));
    }];
    [self.inputText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.infoText.mas_right).offset(10);
        make.right.equalTo(self.view.mas_right).offset(-60);
        make.top.equalTo(self.view.mas_top).offset(160);
        make.height.equalTo(@(40));
    }];
    //内容初始化
    self.inputText.placeholder = @"在此输入你要更改的内容";
    self.infoText.text = @"输入内容";
    self.infoText.font = [UIFont systemFontOfSize:14];
}
-(void)addCommitButton{
    NSLog(@"ww");
    self.commitButton = [[UIButton alloc]init];
    
    [self.view addSubview:self.commitButton];
    
    [self.commitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.infoText.mas_bottom).offset(10);
        make.height.equalTo(@(40));
        make.width.equalTo(@(140));
        make.centerX.equalTo(self.view.mas_centerX);
    }];
    
    [self.commitButton setTitle:@"更新内容" forState:UIControlStateNormal];
    [self.commitButton setTitleColor:[UIColor systemBlueColor] forState:UIControlStateNormal];
    [self.commitButton addTarget:self action:@selector(setTextButton) forControlEvents:UIControlEventTouchUpInside];
    
}
-(void)setTextButton{
    //判断是否存在block
    if(self.setText){
        if(![self.inputText.text isEqual:@""]){
            NSLog(@"aaa");
            self.setText(self.inputText.text);
        }
        else
            self.setText(@"未获取到任何值");
    }else{
        //catch error
        NSLog(@"error");
    }
    [self.navigationController popViewControllerAnimated:true];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
