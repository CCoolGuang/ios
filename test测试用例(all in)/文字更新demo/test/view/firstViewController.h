//
//  firstViewController.h
//  testView
//
//  Created by bytedance on 2021/10/9.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface firstViewController : UIViewController
@property(nonatomic,strong) UILabel* textLabel;
@property(nonatomic,strong) UIButton* update;

@end

NS_ASSUME_NONNULL_END
