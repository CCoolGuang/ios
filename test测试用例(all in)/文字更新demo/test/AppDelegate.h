//
//  AppDelegate.h
//  testNav
//
//  Created by bytedance on 2021/10/9.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (readonly, strong) NSPersistentContainer *persistentContainer;
@property (nonatomic,strong) UIWindow *window;

- (void)saveContext;


@end

